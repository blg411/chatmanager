# ChatManager
An Enterprise Instant Messaging abstraction API

## Installation 
```sh
npm install chatmanager --save
yarn add chatmanager
bower install chatmanager --save
```

## Usage

### Javascript

```javascript
var chatmanager = require('chatmanager');
var test = chatmanager.getTest('first');
```
```sh
Output should be 'testfirst'
```

### TypeScript

```typescript
import { getTest } from 'chatmanager';
console.log(getTest('second'))
```
```sh
Output should be 'testsecond'
```

### AMD

```javascript
define(function(require,exports,module){
  var chatmanager = require('chatmanager');
});
```

## Test 

```sh
npm run test
```

## Contributors:
- Umut Senliol
- Doguhan Bozbey
- Taha Samet Aydil
- Emre Horsanalı
