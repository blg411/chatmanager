/**
* @Method: Returns test string.
* @Param {string}
* @Return {string}
*/
export function getTest (str: any) : string {
  return "test" + str
}
